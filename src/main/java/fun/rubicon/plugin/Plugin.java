/*
 * RubiconBot - An open source Discord bot
 * Copyright (C) 2018  RubiconBot Dev Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package fun.rubicon.plugin;

import fun.rubicon.plugin.command.Command;
import net.dv8tion.jda.core.hooks.ListenerAdapter;

public abstract class Plugin {
    private boolean enabled = false;
    private Rubicon rubicon;
    private PluginMetadata pluginMetadata;

    public void init() {
    }

    public void onEnable() {
    }

    public void onDisable() {
    }

    public final void enable() {
        if (enabled)
            return;
        enabled = true;
        onEnable();
    }

    public final void disable() {
        if (!enabled)
            return;
        enabled = false;
        onDisable();
    }

    protected final void registerCommand(Command command) {
        rubicon.registerCommand(this, command);
    }

    protected final void unregisterCommand(Command command) {
        rubicon.unregisterCommand(this, command);
    }

    protected final void registerListener(ListenerAdapter listenerAdapter) {
        rubicon.registerListener(this, listenerAdapter);
    }

    protected final void unregisterListener(ListenerAdapter listenerAdapter) {
        rubicon.unregisterListener(this, listenerAdapter);
    }

    public final void setRubicon(Rubicon rubicon) {
        this.rubicon = rubicon;
    }

    public final void setPluginMetadata(PluginMetadata pluginMetadata) {
        this.pluginMetadata = pluginMetadata;
    }

    @Deprecated
    public final PluginMetadata getPluginMetadata() {
        return this.pluginMetadata;
    }

    public final PluginMetadata getMetaData() {
        return this.pluginMetadata;
    }

    /**
     * @return whether this plugin is enabled.
     */
    public final boolean isEnabled() {
        return enabled;
    }

    /**
     * @return the Rubicon instance that loaded this plugin.
     */
    public final Rubicon getRubicon() {
        return rubicon;
    }
}
