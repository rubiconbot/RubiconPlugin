/*
 * RubiconBot - A open source Discord bot
 * Copyright (C) 2018  RubiconBot Dev Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package fun.rubicon.plugin;

import com.datastax.driver.core.Session;
import fun.rubicon.plugin.command.Command;
import fun.rubicon.plugin.io.Config;
import lavalink.client.io.Lavalink;
import net.dv8tion.jda.bot.sharding.ShardManager;
import net.dv8tion.jda.core.hooks.ListenerAdapter;

import java.util.List;

public interface Rubicon {

    Rubicon registerCommand(Plugin plugin, Command command);

    Rubicon unregisterCommand(Plugin plugin, Command command);

    Rubicon registerListener(Plugin plugin, ListenerAdapter adapter);

    Rubicon unregisterListener(Plugin plugin, ListenerAdapter adapter);

    ShardManager getShardManager();

    String getVersion();

    List<Command> getCommands();

    Config getConfig();

    Session getCassandra();

    Lavalink getLavalink();
}
