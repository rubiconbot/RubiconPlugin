/*
 * RubiconBot - A open source Discord bot
 * Copyright (C) 2018  RubiconBot Dev Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package fun.rubicon.plugin.entities.provider;

import com.datastax.driver.mapping.Mapper;
import fun.rubicon.plugin.entities.RubiconGuild;
import fun.rubicon.plugin.io.db.Cassandra;

public class GuildProvider {

    public static RubiconGuild getGuild(long guildId) {
        Mapper<RubiconGuild> mapper = Cassandra.getCassandra().getMappingManager().mapper(RubiconGuild.class);
        return mapper.get(guildId);
    }

    public static void createTable() {
        Cassandra.getCassandra().getSession().execute("CREATE TABLE IF NOT EXISTS guilds(guild_id bigint PRIMARY KEY, prefix text);");
    }
}
