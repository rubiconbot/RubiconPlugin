/*
 * RubiconBot - A open source Discord bot
 * Copyright (C) 2018  RubiconBot Dev Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package fun.rubicon.plugin.entities;

import com.datastax.driver.mapping.MappingManager;
import com.datastax.driver.mapping.annotations.Column;
import com.datastax.driver.mapping.annotations.PartitionKey;
import com.datastax.driver.mapping.annotations.Table;
import com.datastax.driver.mapping.annotations.Transient;
import fun.rubicon.plugin.io.db.Cassandra;
import fun.rubicon.plugin.io.db.DatabaseEntity;
import lombok.Data;

import java.util.function.Consumer;

@Table(name = "guilds")
@Data
public class RubiconGuild extends DatabaseEntity<RubiconGuild> {

    @Transient
    private MappingManager mappingManager;
    @PartitionKey
    @Column(name = "guild_id")
    private long guildId;
    private String prefix;

    public RubiconGuild() {
        super(RubiconGuild.class, Cassandra.getCassandra());
    }

    public RubiconGuild(long guildId, String prefix) {
        super(RubiconGuild.class, Cassandra.getCassandra());
        this.guildId = guildId;
        this.prefix = prefix;
    }

    @Override
    public void save() {
        save(this, null, null);
    }

    @Override
    public void save(Consumer<RubiconGuild> onSuccess) {
        save(this, onSuccess, null);
    }

    @Override
    public void save(Consumer<RubiconGuild> onSuccess, Consumer<Throwable> onError) {
        save(this, onSuccess, onError);
    }

    @Override
    public void delete() {
        delete(this, null, null);
    }

    @Override
    public void delete(Consumer<RubiconGuild> onSuccess) {
        delete(this, onSuccess, null);
    }

    @Override
    public void delete(Consumer<RubiconGuild> onSuccess, Consumer<Throwable> onError) {
        delete(this, onSuccess, onError);
    }
}