/*
 * RubiconBot - A open source Discord bot
 * Copyright (C) 2018  RubiconBot Dev Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package fun.rubicon.plugin.command;


import lombok.extern.log4j.Log4j;
import net.dv8tion.jda.core.entities.Message;
import net.dv8tion.jda.core.entities.User;
import net.dv8tion.jda.core.events.message.guild.react.GuildMessageReactionAddEvent;
import net.dv8tion.jda.core.hooks.ListenerAdapter;

import java.util.Collection;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;

@Log4j
public class Reactions extends ListenerAdapter {

    private Collection<String> emojis;
    private User user;
    private Message message;
    private int delayToAbort;
    private boolean is = false;
    private Consumer<String> resolve;

    public Reactions(Collection<String> emojis, User user, Message message, int delayToAbort, Consumer<String> resolve) {
        this.emojis = emojis;
        this.user = user;
        this.message = message;
        this.delayToAbort = delayToAbort;
        this.resolve = resolve;
        emojis.forEach((emoji) -> message.addReaction(emoji).queue());
    }


    @Override
    public void onGuildMessageReactionAdd(GuildMessageReactionAddEvent event) {
        if (!is) {
            //Great Idea from https://github.com/CodersClashS01/try-catch
            message.delete().queueAfter(delayToAbort, TimeUnit.SECONDS, (resolve) -> event.getJDA().removeEventListener(this), (reject) -> event.getJDA().removeEventListener(this));
            is = true;
        }
        User user = event.getUser();
        if (user.isBot() || event.getMessageIdLong() != message.getIdLong())
            return;
        event.getReaction().removeReaction(user).queue();
        String reaction = event.getReactionEmote().getName();
        if (user.getIdLong() == this.user.getIdLong())
            if (emojis.contains(reaction))
                this.resolve.accept(reaction);
    }
}
