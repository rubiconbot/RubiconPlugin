/*
 * RubiconBot - A open source Discord bot
 * Copyright (C) 2018  RubiconBot Dev Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package fun.rubicon.plugin.command;

import lombok.Getter;

public enum CommandCategory {

    UNKNOWN(0, "Unknown"),
    MUSIC(1, "Music"),
    BOT_OWNER(2, "BotOwner"),
    MODERATION(3, "Moderation"),
    UTILITY(4, "Utility"),
    SETTINGS(5, "Settings"),
    GAMES(6, "Games"),
    RPG(7, "RPG"),
    INFO(8, "Info");

    @Getter
    private long id;
    @Getter
    private String displayName;

    CommandCategory(long id, String displayName) {
        this.id = id;
        this.displayName = displayName;
    }


}
