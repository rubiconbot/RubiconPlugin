/*
 * RubiconBot - A open source Discord bot
 * Copyright (C) 2018  RubiconBot Dev Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package fun.rubicon.plugin.command;

import fun.rubicon.plugin.Plugin;
import fun.rubicon.plugin.util.Colors;
import fun.rubicon.plugin.util.EmbedUtil;
import lombok.*;
import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.entities.Message;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;


@Data
public abstract class Command extends EmbedUtil {

    private final String[] invocations;
    private final CommandCategory commandCategory;
    private final String usage;
    private final String description;
    private final Map<String, Command> subCommands = new HashMap<>();
    private Plugin plugin;
    @Getter(AccessLevel.PRIVATE)
    @Setter(AccessLevel.PRIVATE)
    private String usageBase;

    public Command(@NonNull String[] invocations, @NonNull CommandCategory commandCategory, @NonNull String usage, @NonNull String description) {
        if (invocations.length == 0)
            throw new RuntimeException("Invocations cannot be empty.");
        this.invocations = invocations;
        this.commandCategory = commandCategory;
        this.usage = usage;
        this.description = description;
        this.usageBase = invocations[0];
    }

    public Command registerSubCommand(Command command) {
        command.setUsageBase(usageBase + " " + command.getUsageBase());
        Arrays.asList(command.invocations).forEach(invocation -> subCommands.put(invocation, command));
        return this;
    }

    public Command unregisterSubCommand(Command command) {
        Arrays.asList(command.invocations).forEach(subCommands::remove);
        return this;
    }

    public abstract Result execute(CommandEvent event, String[] args);

    protected final Result send(String s) {
        return new Result(s);
    }

    protected final Result send(EmbedBuilder embedBuilder) {
        return new Result(embedBuilder);
    }

    protected final Result send(Message message) {
        return new Result(message);
    }

    protected final void verifyChanges(CommandEvent event, Message message, Consumer<String> resolve) {
        List<String> emojiList = Arrays.asList("\u274c");
        Reactions reactions = new Reactions(emojiList, event.getAuthor(), message, 15, resolve);
        event.getAuthor().getJDA().addEventListener(reactions);
    }

    protected final void verifyChanges(CommandEvent event, Message message, int delay, Consumer<String> resolve) {
        List<String> emojiList = Arrays.asList("\\u274c");
        Reactions reactions = new Reactions(emojiList, event.getAuthor(), message, delay, resolve);
        event.getAuthor().getJDA().addEventListener(reactions);
    }

    public final Result sendHelp(CommandEvent commandEvent) {
        EmbedBuilder embedBuilder = new EmbedBuilder();
        embedBuilder.setAuthor(String.format("Command Help - %s", invocations[0]));
        embedBuilder.setDescription(description);
        embedBuilder.setColor(Colors.BLUE);
        embedBuilder.addField("Usage", generateUsage(this, commandEvent.getPrefix() + usageBase), false);
        embedBuilder.setFooter(String.format("Requested by %s#%s", commandEvent.getAuthor().getName(), commandEvent.getAuthor().getDiscriminator()), commandEvent.getAuthor().getAvatarUrl());
        return send(embedBuilder);
    }

    public String generateUsage(Command command, String usageBase) {
        StringBuilder usage = new StringBuilder();
        usage.append(usageBase).append(" ").append(command.usage);
        for (Command subCommand : command.getSubCommands().values())
            usage.append("\n").append(generateUsage(subCommand, usageBase + " " + subCommand.invocations[0]));
        return usage.toString();
    }
}
