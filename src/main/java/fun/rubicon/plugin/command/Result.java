/*
 * RubiconBot - A open source Discord bot
 * Copyright (C) 2018  RubiconBot Dev Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package fun.rubicon.plugin.command;

import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.MessageBuilder;
import net.dv8tion.jda.core.entities.Message;

public class Result {

    private String string = null;
    private EmbedBuilder embedBuilder = null;
    private Message message = null;

    public Result(String string) {
        this.string = string;
    }

    public Result(EmbedBuilder embedBuilder) {
        this.embedBuilder = embedBuilder;
    }

    public Result(Message message) {
        this.message = message;
    }

    public Message build() {
        Message msg;
        if (string != null) {
            msg = new MessageBuilder(string).build();
        } else if (embedBuilder != null) {
            msg = new MessageBuilder().setEmbed(embedBuilder.build()).build();
        } else if (message != null) {
            msg = message;
        } else
            throw new RuntimeException("Message returnable cannot be null.");
        return msg;
    }
}
