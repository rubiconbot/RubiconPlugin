/*
 * RubiconBot - A open source Discord bot
 * Copyright (C) 2018  RubiconBot Dev Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package fun.rubicon.plugin.io.db;

import com.datastax.driver.mapping.Mapper;
import com.datastax.driver.mapping.MappingManager;
import com.datastax.driver.mapping.annotations.Transient;
import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;

import javax.annotation.Nullable;
import java.util.function.Consumer;

public abstract class DatabaseEntity<T> {

    @Transient
    private final Mapper<T> mapper;
    @Transient
    private final Consumer<T> DEFAULT_SUCCESS = (a) -> {
    };
    @Transient
    private final transient Consumer<Throwable> DEFAULT_ERROR = Throwable::printStackTrace;

    public DatabaseEntity(Class<T> clazz, Cassandra cassandra) {
        MappingManager mappingManager = new MappingManager(cassandra.getSession());
        this.mapper = mappingManager.mapper(clazz);
    }

    public abstract void save();

    public abstract void save(Consumer<T> onSuccess);

    public abstract void save(Consumer<T> onSuccess, Consumer<Throwable> onError);

    protected final void save(T entity, Consumer<T> onSuccess, Consumer<Throwable> onError) {
        final Consumer<T> success = onSuccess != null ? onSuccess : DEFAULT_SUCCESS;
        final Consumer<Throwable> error = onError != null ? onError : DEFAULT_ERROR;

        ListenableFuture<Void> future = mapper.saveAsync(entity);
        Futures.addCallback(future, new FutureCallback<Void>() {

            @Override
            public void onSuccess(@Nullable Void result) {
                success.accept(entity);
            }

            @Override
            public void onFailure(@Nullable Throwable t) {
                error.accept(t);
            }
        });
    }

    public abstract void delete();

    public abstract void delete(Consumer<T> onSuccess);

    public abstract void delete(Consumer<T> onSuccess, Consumer<Throwable> onError);

    protected final void delete(T entity, Consumer<T> onSuccess, Consumer<Throwable> onError) {
        final Consumer<T> success = onSuccess != null ? onSuccess : DEFAULT_SUCCESS;
        final Consumer<Throwable> error = onError != null ? onError : DEFAULT_ERROR;

        ListenableFuture<Void> future = mapper.deleteAsync(entity);
        Futures.addCallback(future, new FutureCallback<Void>() {

            @Override
            public void onSuccess(@Nullable Void result) {
                success.accept(entity);
            }

            @Override
            public void onFailure(@Nullable Throwable t) {
                error.accept(t);
            }
        });
    }
}
