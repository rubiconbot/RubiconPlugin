/*
 * RubiconBot - A open source Discord bot
 * Copyright (C) 2018  RubiconBot Dev Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package fun.rubicon.plugin.io.db;

import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.PlainTextAuthProvider;
import com.datastax.driver.core.Session;
import com.datastax.driver.mapping.MappingManager;
import lombok.Getter;
import lombok.extern.log4j.Log4j;

@Log4j
public class Cassandra {

    private final String user;
    private final String password;
    private final String[] contactPoints;
    @Getter
    private Cluster cluster;
    @Getter
    private Session session;
    @Getter
    private MappingManager mappingManager;
    @Getter
    private static Cassandra cassandra;

    public Cassandra(String user, String password, String[] contactPoints) {
        this.user = user;
        this.password = password;
        this.contactPoints = contactPoints;
    }

    public void connect(String keyspace) {
        Cluster.Builder builder = Cluster.builder();
        builder.addContactPoints(contactPoints);
        builder.withAuthProvider(new PlainTextAuthProvider(user, password));
        cluster = builder.build();
        session = cluster.connect(keyspace);
        mappingManager = new MappingManager(session);
        cassandra = this;
        log.info("Connection to cassandra successful");
    }

    public Session getConnection() {
        return session;
    }

    public void close() {
        session.close();
        cluster.close();
    }
}