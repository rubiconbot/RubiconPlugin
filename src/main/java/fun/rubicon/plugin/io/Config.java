/*
 * RubiconBot - A open source Discord bot
 * Copyright (C) 2018  RubiconBot Dev Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package fun.rubicon.plugin.io;

import lombok.extern.log4j.Log4j;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

@Log4j
public class Config extends JSONObject {

    private final File configFile;

    /**
     * Creates a {@link Config} object with a custom file
     *
     * @param file the file where the config should be located
     */
    public Config(File file) {
        this.configFile = file;

        if (!configFile.exists())
            try {
                configFile.createNewFile();
            } catch (IOException e) {
                log.error("Error while creating config file.", e);
            }
        load();
    }

    /**
     * Add key-/value pairs to the config
     *
     * @param key   the key
     * @param value the value
     */
    public void set(String key, Object value) {
        put(key, value);
        save();
    }

    /**
     * Does the same as {@link Config#set(String, Object)} but only, if the key does not exist in the current objects
     *
     * @param key   the key
     * @param value the value
     */
    public void setDefault(String key, Object value) {
        if (!has(key))
            set(key, value);
    }

    /**
     * Saves the config to a file
     */
    public void save() {
        try (FileWriter fileWriter = new FileWriter(configFile)) {
            fileWriter.write(toString(2));
        } catch (IOException e) {
            log.error("Error while saving config.", e);
        }
    }

    private void load() {
        try (Scanner scanner = new Scanner(configFile)) {
            StringBuilder stringBuilder = new StringBuilder();
            while (scanner.hasNextLine())
                stringBuilder.append(scanner.nextLine());
            String configContent = stringBuilder.toString();
            if (!configContent.equals("")) {
                JSONObject jsonObject = new JSONObject(configContent);
                for (String key : jsonObject.keySet()) {
                    put(key, jsonObject.get(key));
                }
            }
        } catch (FileNotFoundException e) {
            log.error("Error while reading config file.", e);
        }
    }
}
