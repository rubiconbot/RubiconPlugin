package fun.rubicon.plugin.util.setup;

import net.dv8tion.jda.core.entities.Member;
import net.dv8tion.jda.core.entities.Message;
import net.dv8tion.jda.core.events.message.guild.react.GuildMessageReactionAddEvent;

public abstract class ReactionSetupRequest extends SetupRequest {

    /**
     * @param infoMessage The first info message of the setup
     * @param author      The Author of the invocation message
     * @throws IllegalStateException When the author do already own a SetupRequest
     */
    public ReactionSetupRequest(Message infoMessage, Member author) {
        super(infoMessage, author);
    }

    abstract void handleReaction(GuildMessageReactionAddEvent event, Integer step);

}
