package fun.rubicon.plugin.util.setup;

import net.dv8tion.jda.core.entities.Member;
import net.dv8tion.jda.core.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.core.events.message.guild.react.GuildMessageReactionAddEvent;
import net.dv8tion.jda.core.hooks.ListenerAdapter;

import java.util.HashMap;


public class SetupListener extends ListenerAdapter {

    public static HashMap<Member, SetupRequest> setupStorage = new HashMap<>();

    @Override
    public void onGuildMessageReceived(GuildMessageReceivedEvent event) {
        if(!setupStorage.containsKey(event.getMember()))
            return;
        SetupRequest request = setupStorage.get(event.getMember());
        if(event.getMessage().getContentDisplay().equals("cancel")){
            request.close();
            request.getInfoMessage().delete().queue();
        }
        request.handleStep(event, request.getStep());
    }

    @Override
    public void onGuildMessageReactionAdd(GuildMessageReactionAddEvent event) {
        if(!setupStorage.containsKey(event.getMember()))
            return;
        SetupRequest request = setupStorage.get(event.getMember());
        if(request instanceof ReactionSetupRequest)
            ((ReactionSetupRequest) request).handleReaction(event, request.getStep());
    }
}
