package fun.rubicon.plugin.util.setup;

import fun.rubicon.plugin.util.EmbedUtil;
import lombok.Getter;
import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.MessageBuilder;
import net.dv8tion.jda.core.Permission;
import net.dv8tion.jda.core.entities.Member;
import net.dv8tion.jda.core.entities.Message;
import net.dv8tion.jda.core.entities.MessageEmbed;
import net.dv8tion.jda.core.entities.TextChannel;
import net.dv8tion.jda.core.events.message.guild.GuildMessageReceivedEvent;

@Getter
public abstract class SetupRequest extends EmbedUtil {

    private Message infoMessage;
    private TextChannel setupChannel;
    private Member author;
    private Integer step = 1;

    public abstract void handleStep(GuildMessageReceivedEvent event, Integer step);

    /**
     *
     * @param infoMessage The first info message of the setup
     * @param author The Author of the invocation message
     * @throws IllegalStateException When the author do already own a SetupRequest
     */
    public SetupRequest(Message infoMessage, Member author){
        this.infoMessage = infoMessage;
        this.author = author;
        this.setupChannel = infoMessage.getTextChannel();
        if(SetupListener.setupStorage.containsKey(author))
            throw new IllegalStateException("This member does already own a setup");
        SetupListener.setupStorage.put(author, this);
    }

    private void updateMessage(Message message){
        if(message.getEmbeds().isEmpty() || (!infoMessage.getGuild().getSelfMember().hasPermission(setupChannel, Permission.MESSAGE_EMBED_LINKS) && !message.getEmbeds().isEmpty()))
            infoMessage.editMessage(message).queue();
        else
            infoMessage.editMessage(formatEmbed(message.getEmbeds().get(0))).queue();
    }

    /**
     * Should be called whenever an setup step got executed successfully
     */
    public void next(){
        step++;
        update();
    }

    private void update(){
        SetupListener.setupStorage.replace(author, this);
    }

    public void close(){
        SetupListener.setupStorage.remove(author, this);
    }

    public EmbedBuilder formatStepMessage(){
        return new EmbedBuilder().setFooter("To cancel this setup typein 'cancel' in this channel", null);
    }

    /**
     * Updates the infoMessage of the setup
     * @param text The new content of the message
     */
    private void updateMessage(String text) {
        updateMessage(new MessageBuilder().setContent(text).build());
    }

    /**
     * Updates the infoMessage of the setup
     * @param embed The new content of the message
     */
    private void updateMessage(MessageEmbed embed) {
        updateMessage(new MessageBuilder().setEmbed(embed).build());
    }

    /**
     * Updates the infoMessage of the setup
     * @param builder The new content of the message
     */
    private void updateMessage(EmbedBuilder builder){
        updateMessage(builder.build());
    }

}
