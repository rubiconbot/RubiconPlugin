/*
 * RubiconBot - A open source Discord bot
 * Copyright (C) 2018  RubiconBot Dev Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package fun.rubicon.plugin.util;

import java.util.Calendar;
import java.util.Date;

public class StringUtil {

    public static final char ZERO_WIDTH_CHAR = '\u200b';

    public static Date parseDate(String date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        int amount = parseInt(date);
        if (amount == 0) return null;
        if (date.contains("d"))
            cal.add(Calendar.DAY_OF_MONTH, amount);
        else if (date.contains("m"))
            cal.add(Calendar.MINUTE, amount);
        else if (date.contains("y"))
            cal.add(Calendar.YEAR, amount);
        else if (date.contains("M"))
            cal.add(Calendar.MONTH, amount);
        else if (date.contains("h"))
            cal.add(Calendar.HOUR_OF_DAY, amount);
        else if (date.contains("s"))
            cal.add(Calendar.SECOND, amount);
        else if (date.contains("w"))
            cal.add(Calendar.WEEK_OF_MONTH, amount);
        else
            return null;
        return cal.getTime();
    }

    private static int parseInt(String integer) {
        try {
            return Integer.parseInt(integer.replace("d", "").replace("m", "").replace("y", "").replace("M", "").replace("h", ""));
        } catch (NumberFormatException e) {
            return 0;
        }
    }

}
