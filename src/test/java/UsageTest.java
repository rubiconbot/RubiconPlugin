/*
 * RubiconBot - A open source Discord bot
 * Copyright (C) 2018  RubiconBot Dev Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import fun.rubicon.plugin.command.Command;
import org.junit.Before;
import org.junit.Test;

public class UsageTest {

    private Command testCommand;
    private Command subTestCommand;

    @Before
    public void initCommand() {
        testCommand = new TestCommand();

        subTestCommand = new SubTestCommand();
        subTestCommand.registerSubCommand(new SubSubTestCommand());

        testCommand.registerSubCommand(subTestCommand);
        //testCommand.registerSubCommand(new SubSubTestCommand());
    }

    @Test
    public void testGeneration() {
        System.out.println(subTestCommand.generateUsage(subTestCommand, "rc!test sub"));
    }
}
